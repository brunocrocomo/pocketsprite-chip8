#include "8bkc-hal.h"        // PocketSprite Harware Abstraction Layer        
#include "8bkc-ugui.h"       // PocketSprite uGUI Adapter      
#include "8bkcgui-widgets.h" // PocketSprite uGUI Widgets
#include "appfs.h"           // PocketSprite AppFS
#include "powerbtn_menu.h"   // PocketSprite Power Button Menu header

#include "ugui.h"            // Full uGUI library - Full uGUI reference guide: http://embeddedlightning.com/download/reference-guide/

static int powerbtn_menu_handler() {
    // Call the powerbutton menu and get the user input
    int powerbtn_input = powerbtn_menu_show(kcugui_get_fb()); 

    // Handle the user input from the power button menu
    if (powerbtn_input == POWERBTN_MENU_EXIT) { 
        kchal_exit_to_chooser();
    } else if (powerbtn_input == POWERBTN_MENU_POWERDOWN) {
        kchal_power_down();
    }

    return 0;
}

void app_main() {
    // Initialize the PocketSprite SDK
    kchal_init();

    // Initialize uGUI
    kcugui_init();

    while (1) {
        // Call the file chooser and get the selected rom file
        appfs_handle_t rom_file = kcugui_filechooser("*.*", "SELECT ROM", powerbtn_menu_handler, NULL, 0);
    }
}
